import React from "react";
import SubTitleNote from "./SubTitleNote";

class SubTitle extends React.Component {
  render() {
    return (
      <h2>
        {this.props.text}
        <SubTitleNote
          text="Manda mais repetição!"
          feeling={this.props.feeling}
        />
      </h2>
    );
  }
}

export default SubTitle;
